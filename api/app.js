const express = require("express");
const cors = require('cors');
require('dotenv').config();
const app = express();

app.set('x-powered-by', false);
require('./src/db/mongoose.db');
require('./src/middlewares/passport');

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(require('./src/routes/routes'));

app.use((err, req, res, next) => {
  if (err && err.error && err.error.isJoi) {
    res.status(400).json({
      message: err.error.message,
    });
  } else {
    res.status(err.status || 500).json({ message: err.message || 'Something wrong!' });
  }
});

app.route('').get( (req, res) => {
  res.send('Hello from Node.js RESTful APIsss');
});

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`PORT RUNNING: ${PORT}`);
});
