require("dotenv").config();
const passport = require("passport");
const bcrypt = require("bcryptjs");
const { Strategy: JwtStrategy, ExtractJwt } = require("passport-jwt");
const LocalStrategy = require("passport-local").Strategy;

const {
  findUserAdminByUsernameAuth,
  findUserAdminByIDAuth,
} = require("../services/auth.service");

passport.use(
  new LocalStrategy(
    {
      usernameField: "username",
      passwordField: "password",
      passReqToCallback: true,
    },
    async (req, username, password, next) => {
      try {
        // const { role } = req.body
        let user;
        user = await findUserAdminByUsernameAuth({ username });
        if (!user.status) {
          return next(null, false, "Invalid username or password.");
        }

        const result = await bcrypt.compare(password, user.result.password);
        if (!result) {
          return next(null, false, "Invalid password.");
        }
        return next(null, user.result);
      } catch (error) {
        return next(error);
      }
    }
  )
);

passport.use(
  new JwtStrategy(
    {
      secretOrKey: process.env.AUTH_TOKEN_SECRET,
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    },
    async (payload, next) => {
      if (payload.role == "admin" || payload.role == "superadmin") {
        return await findUserAdminByIDAuth(payload)
          .then((user) => {
            return next(null, user);
          })
          .catch((err) => {
            return next(err);
          });
      } else return next(null, false, "Invalid username or password.");
    }
  )
);
