require('dotenv').config()
const jwt = require('jsonwebtoken')

const accessToken = (payload) => {
  return jwt.sign(payload, process.env.AUTH_TOKEN_SECRET, {
    expiresIn: '2h',
    algorithm: 'HS256'
  })
}

const refreshToken = (payload) => {
  return jwt.sign(payload, process.env.REFRESH_TOKEN_SECRET, {
    expiresIn: '1d',
    algorithm: 'HS256'
  })
}

module.exports = {
  accessToken,
  refreshToken
}
