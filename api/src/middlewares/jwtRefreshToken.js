require('dotenv').config()
const jwt = require('jsonwebtoken')
const jwtRefreshTokenValidate = async (req, res, next) => {
  try {
    if (!req.body['refresh_token']) return res.sendStatus(401)
    const token = req.body['refresh_token'].replace('Bearer ', '')
    await jwt.verify(
      token,
      process.env.REFRESH_TOKEN_SECRET,
      (err, decoded) => {
        if (err) {
          return res.status(403)
        }
        req.user = decoded
        req.user.refresh_token = token
        delete req.user.exp
        delete req.user.iat
      }
    )

    next()
  } catch (error) {
    return res.status(403)
  }
}

module.exports = {
  jwtRefreshTokenValidate
}
