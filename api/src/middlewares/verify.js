require('dotenv').config()
const request = require('request-promise')

const verifySuperAdmin = (req, res, next) => {
  if (req.user.user_type != 'superadmin') return res.status(403).json(403)

  next()
}

const verifyAdmin = (req, res, next) => {
  if (req.user.user_type != 'admin') return res.status(403).json(403)

  next()
}
const verifyBoth = (req, res, next) => {
  if (req.user.user_type !== 'admin' && req.user.user_type !== 'superadmin') {
    return res.status(403).json(403);
  }
  

  next()
}




module.exports = {
  verifySuperAdmin,
  verifyAdmin,
  verifyBoth
}
