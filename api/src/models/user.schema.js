const { Schema, model } = require('mongoose')

const schema = new Schema(
  {
    username: {
      type: String,
      require: true,
      unique: true
    },
    password: {
      type: String,
      require: true
    },
    first_name: {
      type: String
    },
    last_name: {
      type: String
    },
    phone_number: {
      type: String
    },
    status: {
      type: String,
      enum: ['active', 'suspended', 'deleted'],
      default: 'active'
    },
    user_type: {
      type: String,
      enum: ['superadmin', 'admin'],
      default: 'admin'
    }
  },
  { timestamps: true }

  
)

module.exports = model('user', schema, 'user')
