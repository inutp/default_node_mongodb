module.exports = (status, message, result = null) => {
  return {
    status,
    message,
    result
  }
}
