const UserAdminModel = require("../models/user.schema");
const ResponseCode = require("../utils/responseCode");

const bcrypt = require("bcryptjs");

const createUserAdmin = async (obj) => {
  try {
    const { username } = obj;
    const user = await UserAdminModel.findOne({ username });
    if (user) {
      return ResponseCode(false, "Username นี้ถูกใช้งานแล้ว");
    }

    obj.password = await bcrypt.hash(obj.password, 10);

    const result = await UserAdminModel.create(obj);
    return ResponseCode(true, "เพิ่มข้อมูลเจ้าหน้าที่เรียบร้อยแล้ว", result);
  } catch (error) {
    return ResponseCode(false, "ขออภัย กรุณาลองใหม่อีกครั้ง");
  }
};

const findUserAdminByID = async (obj) => {
  const { id } = obj;
  try {
    const user = await UserAdminModel.findById(id).select([
      "_id",
      "username",
      "first_name",
      "last_name",
      "phone_number",
      "status",
    ])
    .populate("position_id", ["_id", "position_name"]);
    if (user) {
      return ResponseCode(true, "OK", user);
    } else {
      return ResponseCode(false, "Not Found User");
    }
  } catch (error) {
    return ResponseCode(false, "No User");
  }
};



module.exports = {
  createUserAdmin,
  findUserAdminByID,

};
