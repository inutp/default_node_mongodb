const UserAdminModel = require('../models/user.schema')
// const UserMemberModel = require('../models/userMember.schema')
// const UserMerchantModel = require('../models/userMerchant.schema')
const ResponseCode = require('../utils/responseCode')

const findUserAdminByUsernameAuth = async (obj) => {
  const { username } = obj
  const user = await UserAdminModel.findOne({ username })
  if (user) {
    return ResponseCode(true, 'OK', user)
  } else {
    return ResponseCode(false, 'Not Found User')
  }
}

const findUserAdminByIDAuth = async (obj) => {
  const { id } = obj
  const user = await UserAdminModel.findById(id).select([
    '_id',
    'username',
    'first_name',
    'last_name',
    'status',
    'user_type'
  ])

  if (user) {
    return user
  } else {
    return ResponseCode(false, 'Not Found User')
  }
}


module.exports = {
  findUserAdminByUsernameAuth,
  findUserAdminByIDAuth,
}
