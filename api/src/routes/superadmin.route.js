const router = require("express").Router();
const passport = require("passport");
const AuthenController = require("../controllers/authen.controller");
const UserAdminController = require("../controllers/userAdmin.controller");
const verify = require('../middlewares/verify')
const reshTokenMiddleware = require("../middlewares/jwtRefreshToken");

const validator = require("express-joi-validation").createValidator({
  passError: true,
});
const dto = require("../validators/dto");

router.post(
  "/login",
  validator.body(dto.LoginUserAdminDto),
  AuthenController.loginLocalUserAdmin
);
router.post(
  "/auth/refresh",
  reshTokenMiddleware.jwtRefreshTokenValidate,
  AuthenController.refreshTokenUserAdmin
);

router.post(
  "/create/admin",
  validator.body(dto.CreateUserAdminDto),
  // passport.authenticate("jwt", { session: false }),
  // verify.verifySuperAdmin,
  UserAdminController.createUserAdmin
);

module.exports = router
