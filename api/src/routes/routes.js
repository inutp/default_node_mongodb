const router = require('express').Router()

router.use('/wb-superadmin', require('./superadmin.route'))

module.exports = router
