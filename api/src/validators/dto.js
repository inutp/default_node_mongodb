const Joi = require("joi");

const LoginUserAdminDto = Joi.object({
  username: Joi.string().required(),
  password: Joi.string().required(),
});

const CreateUserAdminDto = Joi.object({
  username: Joi.string().required(),
  password: Joi.string().required(),
  first_name: Joi.string().required(),
  last_name: Joi.string().required(),
  phone_number: Joi.string().required(),
});


module.exports = {
  LoginUserAdminDto,
  CreateUserAdminDto
};
