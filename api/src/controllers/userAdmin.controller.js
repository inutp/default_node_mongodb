const UserAdminService = require("../services/userAdmin.service");

const createUserAdmin = async (req, res) => {
  const result = await UserAdminService.createUserAdmin(req.body);
  const code = result.status == true ? 201 : 400;
  res.status(code).json({
    status: result.status,
    message: result.message,
  });
};

module.exports = {
  createUserAdmin
};
