require('dotenv').config()
const passport = require('passport')
const jwt = require('jsonwebtoken')
const jwtGenerateToken = require('../middlewares/jwtGenerateToken')

const loginLocalUserAdmin = (req, res, next) => {
  passport.authenticate(
    'local',
    { session: false },
    async (err, user, info) => {
      if (err) return next(err)
      if (user) {
        if (user.status == 'active') {
          const payload = {
            id: user._id,
            username: user.username,
            first_name: user.first_name,
            last_name: user.last_name,
            role: user.user_type
          }

          const access_token = await jwtGenerateToken.accessToken(payload)
          const refresh_token = await jwtGenerateToken.refreshToken(payload)

          return res.status(200).json({
            result: payload,
            access_token,
            refresh_token
          })
        } else {
          return res.status(422).json({
            message: 'บัญชีผู้ใช้งานนี้ถูกระงับการใช้งานหรือถูกลบแล้ว'
          })
        }
      } else {
        return res.status(400).json({
          message: info
        })
      }
    }
  )(req, res, next)
}
const refreshTokenUserAdmin = async (req, res, next) => {
  try {
    await jwt.verify(
      req.user.refresh_token,
      process.env.REFRESH_TOKEN_SECRET,
      (err, decoded) => {
        if (err) {
          return res.status(401).json({
            status: false,
            message: 'Invalid Token.'
          })
        }
        delete req.user.refresh_token
      }
    )

    const payload = req.user
    const access_token = await jwtGenerateToken.accessToken(payload)
    const refresh_token = await jwtGenerateToken.refreshToken(payload)

    return res.status(200).json({
      access_token,
      refresh_token
    })
  } catch (error) {
    return res.status(401).json({
      status: false,
      message: 'Invalid Access.'
    })
  }
}


module.exports = {
  loginLocalUserAdmin,
  refreshTokenUserAdmin,
}
